<?php
/**
 * Description of loggedInAs
 *
 * @author Clem
 */
class Zend_View_Helper_LoggedInAs extends Zend_View_Helper_Abstract
{
    public function loggedInAs ()
    {
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) 
        {
            $username = $auth->getIdentity()->username;
            $logoutUrl = $this->view->url(array('controller'=>'index',
                'action'=>'logout'), null, true);
            return 'Logged as ' . $username .  '. 
                <a class="btn btn-mini btn-danger" type="button" href="'.$logoutUrl.'">Logout</a>';
        }
        
        $loginUrl = $this->view->url(array('controller'=>'index', 'action'=>'login'));
        return '<a class="btn btn-mini btn-primary" type="button" href="'.$loginUrl.'">Login</a>';
    }
}

?>
