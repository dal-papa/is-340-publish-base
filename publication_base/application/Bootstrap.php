<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initDoctype()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('XHTML1_STRICT');
        $view->headTitle()->setSeparator(' - ')->append('CSULB CBA Academic Information System');
        $view->headLink()->appendStylesheet('/assets/bootstrap/css/bootstrap-cerulean-min.css');
        $view->headLink()->appendStylesheet('/assets/bootstrap/css/bootstrap-responsive.css');
        $view->headLink()->appendStylesheet('/assets/bootstrap/css/datepicker.css');
        $view->headScript()->appendFile('/assets/bootstrap/js/jquery.js');
        $view->headScript()->appendFile('/assets/bootstrap/js/bootstrap.min.js');
        $view->headScript()->appendFile('/assets/bootstrap/js/bootstrap-datepicker.js');
    }
}

