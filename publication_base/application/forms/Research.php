<?php

class Application_Form_Research extends Twitter_Bootstrap_Form_Horizontal
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
        
        $this->setMethod('post');
        
        $this->addElement(
            'text', 'id', array(
                'label' => 'ID:',
                'required' => true,
                'filters'    => array('StringTrim'),
                'attribs' => array('readonly' => true)
            ));
     
        $this->addElement(
            'text', 'research_name', array(
                'label' => 'Name:',
                'required' => true,
                'filters'    => array('StringTrim'),
            ));

        $this->addElement(
            'text', 'year', array(
                'label' => 'Year:',
                'required' => true,
                'filters'    => array('StringTrim'),
            ));
        
        $this->addElement(
            'text', 'title', array(
                'label' => 'Title:',
                'required' => true,
                'filters'    => array('StringTrim'),
            ));
        
        $this->addElement(
                'text', 'facultyName', array(
                    'label'     => 'Faculty author:',
                    'required'  => true,
                    'filters'   => array('StringTrim'),
                    'placeholder' => 'Search by name',
                    'autocomplete'=> 'off'
            ));
        
        $this->addElement(
                'hidden', 'faculty_fid');
        
        $this->addElement('submit', 'edit', array(
            'ignore'   => true,
            'label'    => 'Edit',
            ));
    }


}

