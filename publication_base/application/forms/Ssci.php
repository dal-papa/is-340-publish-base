<?php

class Application_Form_Ssci extends Twitter_Bootstrap_Form_Horizontal
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */

        $this->setMethod('post');

        $this->addElement(
                'text', 'rid', array(
            'label' => 'ID:',
            'required' => true,
            'filters' => array('StringTrim'),
            'attribs' => array('readonly' => true)
        ));

        $this->addElement(
                'text', 'ssci_2003', array(
            'label' => 'Ssci 2003:',
            'required' => false,
            'filters' => array('StringTrim'),
        ));

        $this->addElement(
                'text', 'ssci_2004', array(
            'label' => 'Ssci 2004:',
            'required' => false,
            'filters' => array('StringTrim'),
        ));

        $this->addElement(
                'text', 'ssci_2005', array(
            'label' => 'Ssci 2005:',
            'required' => false,
            'filters' => array('StringTrim'),
        ));

        $this->addElement(
                'text', 'ssci_2006', array(
            'label' => 'Ssci 2006:',
            'required' => false,
            'filters' => array('StringTrim'),
        ));

        $this->addElement(
                'text', 'ssci_2007', array(
            'label' => 'Ssci 2007:',
            'required' => false,
            'filters' => array('StringTrim'),
        ));

        $this->addElement(
                'text', 'ssci_2008', array(
            'label' => 'Ssci 2008:',
            'required' => false,
            'filters' => array('StringTrim'),
        ));

        $this->addElement(
                'text', 'ssci_2009', array(
            'label' => 'Ssci 2009:',
            'required' => false,
            'filters' => array('StringTrim'),
        ));

        $this->addElement(
                'text', 'ssci_2009_5year', array(
            'label' => 'Ssci 2009 5year:',
            'required' => false,
            'filters' => array('StringTrim'),
        ));

        $this->addElement('submit', 'editssci', array(
            'ignore' => true,
            'label' => 'Edit',
        ));
    }

}

