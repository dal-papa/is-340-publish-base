<?php

class Application_Form_Login extends Twitter_Bootstrap_Form_Horizontal
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */

        $this->setMethod('post');
        
        $this->addElement(
            'text', 'username', array(
                'required' => true,
                'filters'    => array('StringTrim'),
                'placeholder'   => 'Username',
            ));
 
        $this->addElement('password', 'password', array(
            'required' => true,
            'placeholder'   => 'Password',
            ));
                
        $this->addElement('submit', 'submit', array(
            'label'      => 'Login',
        ));
 
    }

}

