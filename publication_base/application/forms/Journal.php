<?php

class Application_Form_Journal extends Twitter_Bootstrap_Form_Horizontal
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */

        $this->setMethod('post');

        $this->addElement(
                'text', 'rid', array(
            'label' => 'ID:',
            'required' => true,
            'filters' => array('StringTrim'),
            'attribs' => array('readonly' => true)
        ));

        $this->addElement(
                'text', 'research_name', array(
            'label' => 'Name:',
            'required' => true,
            'filters' => array('StringTrim'),
        ));

        $this->addElement(
                'text', 'discipline', array(
            'label' => 'Discipline:',
            'required' => true,
            'filters' => array('StringTrim'),
        ));

        $this->addElement(
                'text', 'classification', array(
            'label' => 'Classification:',
            'required' => true,
            'filters' => array('StringTrim'),
        ));

        $this->addElement(
                'text', 'date_added', array(
            'label' => 'Date added:',
            'required' => false,
            'filters' => array('StringTrim'),
        ));

        $this->addElement(
                'text', 'prior_classification', array(
            'label' => 'Prior classification:',
            'required' => false,
            'filters' => array('StringTrim'),
        ));

        $this->addElement(
                'text', 'h_index', array(
            'label' => 'H index:',
            'required' => false,
            'filters' => array('StringTrim'),
        ));

        $this->addElement(
                'text', 'g_index', array(
            'label' => 'G index:',
            'required' => false,
            'filters' => array('StringTrim'),
        ));

        $this->addElement(
                'text', 'abs_rank', array(
            'label' => 'Abs rank:',
            'required' => false,
            'filters' => array('StringTrim'),
        ));

        $this->addElement(
                'text', 'abdc_rank', array(
            'label' => 'Abcd rank:',
            'required' => false,
            'filters' => array('StringTrim'),
        ));

        $this->addElement(
                'text', 'start_year', array(
            'label' => 'Start year:',
            'required' => false,
            'filters' => array('StringTrim'),
        ));

        $this->addElement(
                'text', 'average_cites', array(
            'label' => 'Average cites:',
            'required' => false,
            'filters' => array('StringTrim'),
        ));

        $this->addElement('submit', 'editjournal', array(
            'ignore' => true,
            'label' => 'Edit',
        ));
    }

}

