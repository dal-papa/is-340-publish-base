<?php

class Application_Form_Teaching extends Twitter_Bootstrap_Form_Horizontal
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
        
        $this->setMethod('post');
        
        $this->addElement(
            'text', 'id', array(
                'label' => 'ID:',
                'required' => true,
                'filters'    => array('StringTrim'),
                'attribs' => array('readonly' => true)
            ));
     
        $this->addElement(
            'text', 'year', array(
                'label' => 'Year:',
                'required' => true,
                'filters'    => array('StringTrim'),
            ));

        $term = $this->createElement('select', 'term');
        $term->setLabel("Term:")
                ->setMultiOptions(array("spring" => "Spring", "summer" => "Summer", "fall" => "Fall", "winter" => "Winter"))
                ->setRequired(true)
                ->addValidator('NotEmpty', true)
                ->clearDecorators();
        $this->addElement($term);
        
        $this->addElement(
            'text', 'scu', array(
                'label' => 'SCU:',
                'required' => true,
                'filters'    => array('StringTrim'),
            ));
        
        $program = $this->createElement('select', 'program');
        $program->setLabel("Program:")
                ->setMultiOptions(array("mba" => "MBA", "u" => "U", "amba" => "AMBA"))
                ->setRequired(true)
                ->addValidator('NotEmpty', true)
                ->clearDecorators();
        $this->addElement($program);
        
        $this->addElement(
            'text', 'faculty_fid', array(
                'label' => 'Faculty_fid:',
                'required' => true,
                'filters'    => array('StringTrim'),
            ));
        
        $this->addElement('submit', 'edit', array(
            'ignore'   => true,
            'label'    => 'Edit',
            ));
    }


}

