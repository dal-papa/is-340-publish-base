<?php

class Application_Form_User extends Twitter_Bootstrap_Form_Horizontal
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */

        $this->setMethod('post');
               
        $this->addElement(
            'text', 'username', array(
                'label' => 'Username:',
                'required' => true,
                'filters' => array('StringTrim'),
            ));
        
        $email = $this->createElement('text', 'email');
        $email->setLabel("Email:")
                ->setRequired(true)
                ->addValidator(new Zend_Validate_EmailAddress())
                ->clearDecorators();
        $this->addElement($email);
         
        $password = $this->createElement('password', 'password', array(
            'label' => 'Password:',
            'required' => true,
            ));
        $password->addValidator('Identical', false, array('token' => 'password_confirm'));
        $password->addErrorMessage("The passwords doesn't match");
        $this->addElement($password);
        
        $this->addElement('password', 'password_confirm', array(
            'label' => 'Confirm password:',
            'required' => true,
            ));
                
        $this->addElement('submit', 'submit', array(
            'label' => 'Sign up',
        ));
    }


}

