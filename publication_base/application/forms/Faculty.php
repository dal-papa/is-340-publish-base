<?php

class Application_Form_Faculty extends Twitter_Bootstrap_Form_Horizontal
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */

        $this->setMethod('post');
        
        $this->addElement(
            'text', 'fid', array(
                'label' => 'ID:',
                'required' => true,
                'filters'    => array('StringTrim'),
                'attribs' => array('readonly' => true)
            ));
        
        $this->addElement(
            'text', 'last', array(
                'label' => 'Last Name:',
                'required' => true,
                'filters'    => array('StringTrim'),
            ));
        
        $this->addElement(
            'text', 'first', array(
                'label' => 'First Name:',
                'required' => true,
                'filters'    => array('StringTrim'),
            ));

        $this->addElement(
            'text', 'dob', array(
                'label' => 'Date of Birth:',
                'required' => true,
                'filters'    => array('StringTrim'),
            ));

        $this->addElement(
            'text', 'rank', array(
                'label' => 'Rank:',
                'filters'    => array('StringTrim'),
            ));        

        $timeBase = $this->createElement('select', 'time_base');
        $timeBase->setLabel("Time base:")
                ->setMultiOptions(array("pt" => "Part time", "ft" => "Full time"))
                ->setRequired(true)
                ->addValidator('NotEmpty', true)
                ->clearDecorators();
        $this->addElement($timeBase);

        $this->addElement(
            'text', 'dept', array(
                'label' => 'Department:',
                'filters'    => array('StringTrim'),
            ));

        $this->addElement(
            'text', 'aqg_aq_pq', array(
                'label' => 'AQG, AQ, PQ:',
                'filters'    => array('StringTrim'),
            ));

        $this->addElement(
            'text', 'p_s', array(
                'label' => 'P_S:',
                'filters'    => array('StringTrim'),
            ));

        $this->addElement(
            'text', 'date_hired', array(
                'label' => 'Date hired:',
                'filters'    => array('StringTrim'),
            ));

        $this->addElement('submit', 'edit', array(
            'ignore'   => true,
            'label'    => 'Edit',
            ));
        
    }


}

