<?php

class SearchController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $this->view->headScript()->appendFile('/assets/bootstrap/js/jquery.chained.remote.min.js');
        $formValues = $this->_request->getPost();
        if ($formValues)
        {
            $className = "Application_Model_Mapper_" . $formValues['table'];
            $classTable = new $className;
            $results = $classTable->findByField($formValues['field'], $formValues['q'], FALSE);
            $this->view->results = $results;
        }
    }

    public function getfieldsAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $query = $this->_request->getParam("table");
        if ($query)
        {
            $className = "Application_Model_DbTable_" . $query;
            $classTable = new $className;
            $cols = $classTable->info(Zend_Db_Table::COLS);
            $data = array();
            foreach ($cols as $value)
            {
                $data[$value] = $value;
            }
            echo json_encode($data);
        }
    }
}

