<?php

class TeachingController extends Zend_Controller_Action
{

     /**
     * Mapper for Teaching Table
     * @var Application_Model_Mapper_Scu 
     */
    private $scuTable;
    
    public function init()
    {
        $this->scuTable = new Application_Model_Mapper_Scu();
    }

    public function indexAction()
    {
        $pageNumber = $this->_request->getParam("page");
        $paginator = $this->scuTable->fetchAllToPaginator();
        $paginator->setCurrentPageNumber($pageNumber);
        $this->view->paginator = $paginator;
    }

    public function createAction()
    {
        // Redirect in case of not logged user.
        if (!Zend_Auth::getInstance()->hasIdentity())
            $this->_redirect('/');
        $form = new Application_Form_Teaching();
        $form->removeElement('id');
        $form->getElement('edit')->setLabel('Create');

        $request = $this->getRequest();
        if ($request->isPost())
        {
            $formValues = $request->getPost();
            if ($form->isValid($formValues))
            {
                $teaching = $this->scuTable->getDbTable()->createRow($formValues);
                if ($teaching->save())
                {
                    $this->view->success = 'You have correctly created this Teaching.';
                }
                else
                {
                    $this->view->error = 'Something went wrong. Please try to create again.';
                }
            }
            else
            {
                $this->view->error = 'You have some errors in the form.';
            }
        }

        $this->view->teachingForm = $form;
    }

    public function readAction()
    {
        $teaching = $this->scuTable->find($this->_request->getParam("teaching"), null);
        $this->view->teaching = $teaching;
    }

    public function updateAction()
    {
        // Redirect in case of not logged user.
        if (!Zend_Auth::getInstance()->hasIdentity())
            $this->_redirect('/');
        $form = new Application_Form_Teaching();

        $request = $this->getRequest();
        if ($request->isPost())
        {
            $formValues = $request->getPost();
            if ($form->isValid($formValues))
            {
                //
                // ?? // find faculty_fid ou id ?
                $teachingToUpdate = $this->scuTable->find($formValues['id'], null);
                $this->scuTable->loadModel($formValues, $teachingToUpdate);
                if ($this->scuTable->save($teachingToUpdate))
                {
                    $this->view->success = 'You have correctly updated this Teaching.';
                }
                else
                {
                    $this->view->error = 'Something went wrong. Please try to update again.';
                }
            }
            else
            {
                $this->view->error = 'You have some errors in the form.';
            }
        }
        $teaching = $this->scuTable->find($this->_request->getParam("teaching"), null)->toArray();
        $form->populate($teaching);
        $this->view->teachingForm = $form;
    }

    public function typeaheadAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $query = $this->_request->getParam("q");
        $limit = $this->_request->getParam("limit");
        $resultList = $this->scuTable->fetchListOfScu($query, $limit);
        echo json_encode($resultList);
    }

}







