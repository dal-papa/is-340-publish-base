<?php

class JournalController extends Zend_Controller_Action
{

    /**
     * Mapper for Research Table
     * @var Application_Model_Mapper_Research
     */
    private $journalTable;

    public function init()
    {
        $this->journalTable = new Application_Model_Mapper_Research();
        $this->ssciTable = new Application_Model_Mapper_Ssci();
    }

    public function indexAction()
    {
        $pageNumber = $this->_request->getParam("page");
        $paginator = $this->journalTable->fetchAllToPaginator();
        $paginator->setCurrentPageNumber($pageNumber);
        $this->view->paginator = $paginator;
    }

    public function createAction()
    {
        // Redirect in case of not logged user.
        if (!Zend_Auth::getInstance()->hasIdentity())
            $this->_redirect('/');
        $form = new Application_Form_Journal();
        $form->removeElement('rid');
        $form->getElement('editjournal')->setLabel('Create');

        $request = $this->getRequest();
        if ($request->isPost())
        {
            $formValues = $request->getPost();
            if ($form->isValid($formValues))
            {
                $journal = $this->journalTable->getDbTable()->createRow($formValues);
                if ($journal->save())
                {
                    $ssci = $this->ssciTable->getDbTable()->createRow();
                    $ssci->research_rid = $journal->rid;
                    if ($ssci->save())
                    {
                        $this->view->success = 'You have correctly created this Journal.';
                    } else
                    {
                        $this->view->error = 'Something went wrong. Please try to create again.';
                    }
                } else
                {
                    $this->view->error = 'Something went wrong. Please try to create again.';
                }
            } else
            {
                $this->view->error = 'You have some errors in the form.';
            }
        }

        $this->view->journalForm = $form;
    }

    public function readAction()
    {
        $journal = $this->journalTable->find($this->_request->getParam("journal"), null);
        $this->view->journal = $journal;
    }

    public function updateAction()
    {
        // Redirect in case of not logged user.
        if (!Zend_Auth::getInstance()->hasIdentity())
            $this->_redirect('/');
        $form = new Twitter_Bootstrap_Form_Horizontal();
        $ssciForm = new Application_Form_Ssci();
        $journalForm = new Application_Form_Journal();
        $form->addSubform($journalForm, 'journalform');
        $form->addSubform($ssciForm, 'ssciform');

        $request = $this->getRequest();
        if ($request->isPost())
        {
            $formValues = $request->getPost();
            if ($request->getParam('editjournal'))
            {
                if ($journalForm->isValid($formValues))
                {
                    $journalToUpdate = $this->journalTable->find($formValues['rid'], null);
                    $this->journalTable->loadModel($formValues, $journalToUpdate);
                    if ($this->journalTable->save($journalToUpdate))
                    {
                        $this->view->success = 'You have correctly updated this Journal.';
                    } else
                    {
                        $this->view->error = 'Something went wrong. Please try to update again.';
                    }
                } else
                {
                    $this->view->error = 'You have some errors in the form.';
                }
            }
            if ($request->getParam('editssci'))
            {
                if ($ssciForm->isValid($formValues))
                {
                    $ssciToUpdate = $this->ssciTable->find($formValues['rid'], null);
                    $this->ssciTable->loadModel($formValues, $ssciToUpdate);
                    $ssciToUpdate->setResearchRid($formValues['rid']);
                    if ($this->ssciTable->save($ssciToUpdate))
                    {
                        $this->view->success = 'You have correctly updated the ssci for this Journal.';
                    } else
                    {
                        $this->view->error = 'Something went wrong. Please try to update the ssci again.';
                    }
                } else
                {
                    $this->view->error = 'You have some errors in the ssci form.';
                }
            }
        }
        $journal = $this->journalTable->find($this->_request->getParam("journal"), null)->toArray();
        $form->populate($journal);
        $ssci = $this->ssciTable->find($this->_request->getParam("journal"), null)->toArray();
        $form->populate($ssci);
        $this->view->journalForm = $form;
    }

    public function deleteAction()
    {
        // Redirect in case of not logged user.
        if (!Zend_Auth::getInstance()->hasIdentity())
            $this->_redirect('/');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $idToDelete = $this->_request->getParam("journal");
        echo $idToDelete;
    }
    
    public function typeaheadAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $query = $this->_request->getParam("q");
        $limit = $this->_request->getParam("limit");
        $resultList = $this->journalTable->fetchListOfNames($query, $limit);
        echo json_encode($resultList);
    }
}

