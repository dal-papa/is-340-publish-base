<?php

class AdminController extends Zend_Controller_Action
{
    /**
     * 
     * @var Application_Model_Mapper_Users
     */
    private $userTable;
    
    public function init()
    {
        $this->userTable = new Application_Model_Mapper_Users();
    }

    public function indexAction()
    {
        // Redirect in case of not logged user.
        if (!Zend_Auth::getInstance()->hasIdentity())
            $this->_redirect('/');
        $pageNumber = $this->_request->getParam("page");
        $paginator = $this->userTable->fetchAllToPaginator();
        $paginator->setCurrentPageNumber($pageNumber);
        $this->view->paginator = $paginator;
    }

    public function createAction()
    {
        // Redirect in case of not logged user.
        if (!Zend_Auth::getInstance()->hasIdentity())
            $this->_redirect('/');
        $form = new Application_Form_User();

        $request = $this->getRequest();
        if ($request->isPost())
        {
            $formValues = $request->getPost();
            if ($form->isValid($formValues))
            {
                $formValues['password'] = sha1($formValues['password']);
                $user = $this->userTable->getDbTable()->createRow($formValues);
                if ($user->save())
                {
                    $this->view->success = 'You have correctly created this User.';
                }
                else
                {
                    $this->view->error = 'Something went wrong. Please try to create again.';
                }
            }
            else
            {
                $this->view->error = 'You have some errors in the form.';
            }
        }

        $this->view->userForm = $form;
    }

    public function deleteAction()
    {
        // Redirect in case of not logged user.
        if (!Zend_Auth::getInstance()->hasIdentity())
            $this->_redirect('/');
        
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $idToDelete = $this->_request->getParam("user");
        $userToDelete = $this->userTable->find($idToDelete, null);
        if ($userToDelete->deleteRowByPrimaryKey() != 0)
        {
            echo $idToDelete;
        }
        else 
        {
            echo 'null';
        }
    }


}





