<?php

class FacultyController extends Zend_Controller_Action
{

    /**
     * Mapper for Faculty Table
     * @var Application_Model_Mapper_Faculty 
     */
    private $facultyTable;

    public function init()
    {
        $this->facultyTable = new Application_Model_Mapper_Faculty();
    }

    public function indexAction()
    {
        $pageNumber = $this->_request->getParam("page");
        $paginator = $this->facultyTable->fetchAllToPaginator();
        $paginator->setCurrentPageNumber($pageNumber);
        $this->view->paginator = $paginator;
    }

    public function createAction()
    {
        // Redirect in case of not logged user.
        if (!Zend_Auth::getInstance()->hasIdentity())
            $this->_redirect('/');
        $form = new Application_Form_Faculty();
        $form->removeElement('fid');
        $form->getElement('edit')->setLabel('Create');

        $request = $this->getRequest();
        if ($request->isPost())
        {
            $formValues = $request->getPost();
            if ($form->isValid($formValues))
            {
                $faculty = $this->facultyTable->getDbTable()->createRow($formValues);
                if ($faculty->save())
                {
                    $this->view->success = 'You have correctly created this Faculty.';
                }
                else
                {
                    $this->view->error = 'Something went wrong. Please try to create again.';
                }
            }
            else
            {
                $this->view->error = 'You have some errors in the form.';
            }
        }

        $this->view->facultyForm = $form;
    }

    public function readAction()
    {
        $faculty = $this->facultyTable->find($this->_request->getParam("faculty"), null);
        $this->view->faculty = $faculty;
    }

    public function updateAction()
    {
        // Redirect in case of not logged user.
        if (!Zend_Auth::getInstance()->hasIdentity())
            $this->_redirect('/');
        $form = new Application_Form_Faculty();

        $request = $this->getRequest();
        if ($request->isPost())
        {
            $formValues = $request->getPost();
            if ($form->isValid($formValues))
            {
                $facultyToUpdate = $this->facultyTable->find($formValues['fid'], null);
                $this->facultyTable->loadModel($formValues, $facultyToUpdate);
                if ($this->facultyTable->save($facultyToUpdate))
                {
                    $this->view->success = 'You have correctly updated this Faculty.';
                }
                else
                {
                    $this->view->error = 'Something went wrong. Please try to update again.';
                }
            }
            else
            {
                $this->view->error = 'You have some errors in the form.';
            }
        }
        $faculty = $this->facultyTable->find($this->_request->getParam("faculty"), null)->toArray();
        $form->populate($faculty);
        $this->view->facultyForm = $form;
    }

    public function deleteAction()
    {
        // Redirect in case of not logged user.
        if (!Zend_Auth::getInstance()->hasIdentity())
            $this->_redirect ('/');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $idToDelete = $this->_request->getParam("faculty");
        $facultyToDelete = $this->facultyTable->find($idToDelete, null);
        require_once (APPLICATION_PATH . '/models/DbTable/Ic.php');
        if ($facultyToDelete->deleteRowByPrimaryKey() != 0)
        {
            echo $idToDelete;
        }
        else 
        {
            echo 'null';
        }
    }
    
    public function typeaheadAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $query = $this->_request->getParam("q");
        $limit = $this->_request->getParam("limit");
        $resultList = $this->facultyTable->fetchListOfNames($query, $limit);
        echo json_encode($resultList);
    }

}
