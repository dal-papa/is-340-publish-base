<?php

class ResearchController extends Zend_Controller_Action
{
    /**
     * Mapper for the Research Table
     * @var type Application_Model_Mapper_Ic
     */
    private $icTable;
    
    public function init()
    {
        $this->icTable = new Application_Model_Mapper_Ic();
    }

    public function indexAction()
    {
        $pageNumber = $this->_request->getParam("page");
        $paginator = $this->icTable->fetchAllToPaginator();
        $paginator->setCurrentPageNumber($pageNumber);
        $this->view->paginator = $paginator;
    }

    public function readAction()
    {
        $research = $this->icTable->find($this->_request->getParam("research"), null);
        $this->view->research = $research;
    }

    public function createAction()
    {
        // Redirect in case of not logged user.
        if (!Zend_Auth::getInstance()->hasIdentity())
            $this->_redirect ('/');
        $form = new Application_Form_Research();
        $form->removeElement('id');
        $form->getElement('edit')->setLabel('Create');

        $request = $this->getRequest();
        if ($request->isPost())
        {
            $formValues = $request->getPost();
            if ($form->isValid($formValues))
            {
                $ic = $this->icTable->getDbTable()->createRow($formValues);
                if ($ic->save())
                {
                    $this->view->success = 'You have correctly created this Research record.';
                }
                else
                {
                    $this->view->error = 'Something went wrong. Please try to create again.';
                }
            }
            else
            {
                $this->view->error = 'You have some errors in the form.';
            }
        }

        $this->view->researchForm = $form;
    }

    public function updateAction()
    {
        // Redirect in case of not logged user.
        if (!Zend_Auth::getInstance()->hasIdentity())
            $this->_redirect ('/');
        
        $form = new Application_Form_Research();
        $request = $this->getRequest();
        if ($request->isPost())
        {
            $formValues = $request->getPost();
            if ($form->isValid($formValues))
            {
                $icToUpdate = $this->icTable->find($formValues['id'], null);
                $this->icTable->loadModel($formValues, $icToUpdate);
                if ($this->icTable->save($icToUpdate))
                {
                    $this->view->success = 'You have correctly updated this Research Record.';
                }
                else
                {
                    $this->view->error = 'Something went wrong. Please try to update again.';
                }
            }
            else
            {
                $this->view->error = 'You have some errors in the form.';
            }
        }
        $researchModel = $this->icTable->find($this->_request->getParam("research"), null);
        $research = $researchModel->toArray();
        $research["facultyName"] = $researchModel->getFacultyF()->first . " " . $researchModel->getFacultyF()->last;
        $form->populate($research);
        $this->view->researchForm = $form;
    }

    public function deleteAction()
    {
        // Redirect in case of not logged user.
        if (!Zend_Auth::getInstance()->hasIdentity())
            $this->_redirect ('/');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $idToDelete = $this->_request->getParam("research");
        echo $idToDelete;
    }

    public function typeaheadAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $query = $this->_request->getParam("q");
        $limit = $this->_request->getParam("limit");
        $resultList = $this->icTable->fetchListOfNames($query, $limit);
        echo json_encode($resultList);
    }
}









