<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity())
        {
            $this->view->identity = $auth->getIdentity();
        }
    }
    
    protected function _getAuthAdapter()
    {
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);

        $authAdapter->setTableName('users')
            ->setIdentityColumn('username')
            ->setCredentialColumn('password')
            ->setCredentialTreatment('SHA1(?)');

        return $authAdapter;
    }
    
    protected function _process($values)
    {

        // Get our authentication adapter and check credentials

        $adapter = $this->_getAuthAdapter();
        $adapter->setIdentity($values['username']);
        $adapter->setCredential($values['password']);

        $auth = Zend_Auth::getInstance();
        $result = $auth->authenticate($adapter);
        if ($result->isValid())
        {
            $user = $adapter->getResultRowObject();
            $userTable = new Application_Model_Mapper_Users();
            $userModel = $userTable->find($user->idusers, null);
            $userModel->setDateConnected(time());
            $userModel->save();
            $auth->getStorage()->write($user);
            return true;
        }
        return false;
    }

    public function loginAction()
    {
        $form = new Application_Form_Login();
        $request = $this->getRequest();

        if ($request->isPost())
        {
            if ($form->isValid($request->getPost()))
            {
                if ($this->_process($form->getValues()))
                {
                    // We're authenticated! Redirect to the home page
                    $this->_helper->redirector('index', 'index');

                }
                else
                {
                    $form->addError("Impossible to connect you with this credentials !");
                }
            }

        }
        $this->view->loginForm = $form;
    }
    
    public function logoutAction()
    {
        $this->_helper->layout()->disableLayout(); 
        $this->_helper->viewRenderer->setNoRender(true);
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        $this->_redirect('/');
    }


}



