<?php

/**
 * Application Models
 *
 * @package Application_Model
 * @subpackage Model
 * @author Florent De Neve
 * @copyright 
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */


/**
 * 
 *
 * @package Application_Model
 * @subpackage Model
 * @author Florent De Neve
 */
class Application_Model_Points extends Application_Model_ModelAbstract
{

    /**
     * Database var type varchar(25)
     *
     * @var string
     */
    protected $_Classification;

    /**
     * Database var type int(11)
     *
     * @var int
     */
    protected $_Points;



    /**
     * Sets up column and relationship lists
     */
    public function __construct()
    {
        parent::init();
        $this->setColumnsList(array(
            'classification'=>'Classification',
            'points'=>'Points',
        ));

        $this->setParentList(array(
        ));

        $this->setDependentList(array(
        ));
    }

    /**
     * Sets column classification
     *
     * @param string $data
     * @return Application_Model_Points
     */
    public function setClassification($data)
    {
        $this->_Classification = $data;
        return $this;
    }

    /**
     * Gets column classification
     *
     * @return string
     */
    public function getClassification()
    {
        return $this->_Classification;
    }

    /**
     * Sets column points
     *
     * @param int $data
     * @return Application_Model_Points
     */
    public function setPoints($data)
    {
        $this->_Points = $data;
        return $this;
    }

    /**
     * Gets column points
     *
     * @return int
     */
    public function getPoints()
    {
        return $this->_Points;
    }

    /**
     * Returns the mapper class for this model
     *
     * @return Application_Model_Mapper_Points
     */
    public function getMapper()
    {
        if ($this->_mapper === null) {
            $this->setMapper(new Application_Model_Mapper_Points());
        }

        return $this->_mapper;
    }

    /**
     * Deletes current row by deleting the row that matches the primary key
     *
	 * @see Application_Model_Mapper_Points::delete
     * @return int|boolean Number of rows deleted or boolean if doing soft delete
     */
    public function deleteRowByPrimaryKey()
    {
        if ($this->getClassification() === null) {
            throw new Exception('Primary Key does not contain a value');
        }

        return $this->getMapper()
                    ->getDbTable()
                    ->delete('classification = ' .
                             $this->getMapper()
                                  ->getDbTable()
                                  ->getAdapter()
                                  ->quote($this->getClassification()));
    }
}
