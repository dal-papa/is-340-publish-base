<?php

/**
 * Application Models
 *
 * @package Application_Model
 * @subpackage Model
 * @author Florent De Neve
 * @copyright 
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */


/**
 * 
 *
 * @package Application_Model
 * @subpackage Model
 * @author Florent De Neve
 */
class Application_Model_Users extends Application_Model_ModelAbstract
{

    /**
     * Database var type int(11)
     *
     * @var int
     */
    protected $_Idusers;

    /**
     * Database var type varchar(100)
     *
     * @var string
     */
    protected $_Username;

    /**
     * Database var type varchar(100)
     *
     * @var string
     */
    protected $_Email;

    /**
     * Database var type varchar(255)
     *
     * @var string
     */
    protected $_Password;

    /**
     * Database var type datetime
     *
     * @var string
     */
    protected $_DateCreated;

    /**
     * Database var type datetime
     *
     * @var string
     */
    protected $_DateConnected;



    /**
     * Sets up column and relationship lists
     */
    public function __construct()
    {
        parent::init();
        $this->setColumnsList(array(
            'idusers'=>'Idusers',
            'username'=>'Username',
            'email'=>'Email',
            'password'=>'Password',
            'date_created'=>'DateCreated',
            'date_connected'=>'DateConnected',
        ));

        $this->setParentList(array(
        ));

        $this->setDependentList(array(
        ));
    }

    /**
     * Sets column idusers
     *
     * @param int $data
     * @return Application_Model_Users
     */
    public function setIdusers($data)
    {
        $this->_Idusers = $data;
        return $this;
    }

    /**
     * Gets column idusers
     *
     * @return int
     */
    public function getIdusers()
    {
        return $this->_Idusers;
    }

    /**
     * Sets column username
     *
     * @param string $data
     * @return Application_Model_Users
     */
    public function setUsername($data)
    {
        $this->_Username = $data;
        return $this;
    }

    /**
     * Gets column username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->_Username;
    }

    /**
     * Sets column email
     *
     * @param string $data
     * @return Application_Model_Users
     */
    public function setEmail($data)
    {
        $this->_Email = $data;
        return $this;
    }

    /**
     * Gets column email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->_Email;
    }

    /**
     * Sets column password
     *
     * @param string $data
     * @return Application_Model_Users
     */
    public function setPassword($data)
    {
        $this->_Password = $data;
        return $this;
    }

    /**
     * Gets column password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->_Password;
    }

    /**
     * Sets column date_created. Stored in ISO 8601 format.
     *
     * @param string|Zend_Date $date
     * @return Application_Model_Users
     */
    public function setDateCreated($data)
    {
        if (! empty($data)) {
            if (! $data instanceof Zend_Date) {
                $data = new Zend_Date($data);
            }

            $data = $data->toString(Zend_Date::ISO_8601);
        }

        $this->_DateCreated = $data;
        return $this;
    }

    /**
     * Gets column date_created
     *
     * @param boolean $returnZendDate
     * @return Zend_Date|null|string Zend_Date representation of this datetime if enabled, or ISO 8601 string if not
     */
    public function getDateCreated($returnZendDate = false)
    {
        if ($returnZendDate) {
            if ($this->_DateCreated === null) {
                return null;
            }

            return new Zend_Date($this->_DateCreated, Zend_Date::ISO_8601);
        }

        return $this->_DateCreated;
    }

    /**
     * Sets column date_connected. Stored in ISO 8601 format.
     *
     * @param string|Zend_Date $date
     * @return Application_Model_Users
     */
    public function setDateConnected($data)
    {
        if (! empty($data)) {
            if (! $data instanceof Zend_Date) {
                $data = new Zend_Date($data);
            }

            $data = $data->toString(Zend_Date::ISO_8601);
        }

        $this->_DateConnected = $data;
        return $this;
    }

    /**
     * Gets column date_connected
     *
     * @param boolean $returnZendDate
     * @return Zend_Date|null|string Zend_Date representation of this datetime if enabled, or ISO 8601 string if not
     */
    public function getDateConnected($returnZendDate = false)
    {
        if ($returnZendDate) {
            if ($this->_DateConnected === null) {
                return null;
            }

            return new Zend_Date($this->_DateConnected, Zend_Date::ISO_8601);
        }

        return $this->_DateConnected;
    }

    /**
     * Returns the mapper class for this model
     *
     * @return Application_Model_Mapper_Users
     */
    public function getMapper()
    {
        if ($this->_mapper === null) {
            $this->setMapper(new Application_Model_Mapper_Users());
        }

        return $this->_mapper;
    }

    /**
     * Deletes current row by deleting the row that matches the primary key
     *
	 * @see Application_Model_Mapper_Users::delete
     * @return int|boolean Number of rows deleted or boolean if doing soft delete
     */
    public function deleteRowByPrimaryKey()
    {
        if ($this->getIdusers() === null) {
            throw new Exception('Primary Key does not contain a value');
        }

        return $this->getMapper()
                    ->getDbTable()
                    ->delete('idusers = ' .
                             $this->getMapper()
                                  ->getDbTable()
                                  ->getAdapter()
                                  ->quote($this->getIdusers()));
    }
}
