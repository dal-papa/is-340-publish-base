<?php

/**
 * Application Models
 *
 * @package Application_Model
 * @subpackage Model
 * @author Florent De Neve
 * @copyright 
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */


/**
 * 
 *
 * @package Application_Model
 * @subpackage Model
 * @author Florent De Neve
 */
class Application_Model_Faculty extends Application_Model_ModelAbstract
{

    /**
     * Database var type int(11)
     *
     * @var int
     */
    protected $_Fid;

    /**
     * Database var type varchar(42)
     *
     * @var string
     */
    protected $_Last;

    /**
     * Database var type varchar(42)
     *
     * @var string
     */
    protected $_First;

    /**
     * Database var type date
     *
     * @var string
     */
    protected $_Dob;

    /**
     * Database var type enum('Lecturer','Full')
     *
     * @var string
     */
    protected $_Rank;

    /**
     * Database var type enum('ft','pt')
     *
     * @var string
     */
    protected $_TimeBase;

    /**
     * Database var type varchar(10)
     *
     * @var string
     */
    protected $_Dept;

    /**
     * Database var type varchar(10)
     *
     * @var string
     */
    protected $_AqgAqPq;

    /**
     * Database var type enum('p','s')
     *
     * @var string
     */
    protected $_PS;

    /**
     * Database var type date
     *
     * @var string
     */
    protected $_DateHired;



    /**
     * Dependent relation ic_ibfk_1
     * Type: One-to-Many relationship
     *
     * @var Application_Model_Ic
     */
    protected $_Ic;

    /**
     * Sets up column and relationship lists
     */
    public function __construct()
    {
        parent::init();
        $this->setColumnsList(array(
            'fid'=>'Fid',
            'last'=>'Last',
            'first'=>'First',
            'dob'=>'Dob',
            'rank'=>'Rank',
            'time_base'=>'TimeBase',
            'dept'=>'Dept',
            'aqg_aq_pq'=>'AqgAqPq',
            'p_s'=>'PS',
            'date_hired'=>'DateHired',
        ));

        $this->setParentList(array(
        ));

        $this->setDependentList(array(
            'IcIbfk1' => array(
                    'property' => 'Ic',
                    'table_name' => 'Ic',
                ),
        ));
    }

    /**
     * Sets column fid
     *
     * @param int $data
     * @return Application_Model_Faculty
     */
    public function setFid($data)
    {
        $this->_Fid = $data;
        return $this;
    }

    /**
     * Gets column fid
     *
     * @return int
     */
    public function getFid()
    {
        return $this->_Fid;
    }

    /**
     * Sets column last
     *
     * @param string $data
     * @return Application_Model_Faculty
     */
    public function setLast($data)
    {
        $this->_Last = $data;
        return $this;
    }

    /**
     * Gets column last
     *
     * @return string
     */
    public function getLast()
    {
        return $this->_Last;
    }

    /**
     * Sets column first
     *
     * @param string $data
     * @return Application_Model_Faculty
     */
    public function setFirst($data)
    {
        $this->_First = $data;
        return $this;
    }

    /**
     * Gets column first
     *
     * @return string
     */
    public function getFirst()
    {
        return $this->_First;
    }
    
    /**
     * Returns the full name
     * @return string
     */
    public function getFullName()
    {
        return $this->_First . " " .$this->_Last;
    }

    /**
     * Sets column dob
     *
     * @param string $data
     * @return Application_Model_Faculty
     */
    public function setDob($data)
    {
        $this->_Dob = $data;
        return $this;
    }

    /**
     * Gets column dob
     *
     * @return string
     */
    public function getDob()
    {
        return $this->_Dob;
    }

    /**
     * Sets column rank
     *
     * @param string $data
     * @return Application_Model_Faculty
     */
    public function setRank($data)
    {
        $this->_Rank = $data;
        return $this;
    }

    /**
     * Gets column rank
     *
     * @return string
     */
    public function getRank()
    {
        return $this->_Rank;
    }

    /**
     * Sets column time_base
     *
     * @param string $data
     * @return Application_Model_Faculty
     */
    public function setTimeBase($data)
    {
        $this->_TimeBase = $data;
        return $this;
    }

    /**
     * Gets column time_base
     *
     * @return string
     */
    public function getTimeBase()
    {
        return $this->_TimeBase;
    }

    /**
     * Sets column dept
     *
     * @param string $data
     * @return Application_Model_Faculty
     */
    public function setDept($data)
    {
        $this->_Dept = $data;
        return $this;
    }

    /**
     * Gets column dept
     *
     * @return string
     */
    public function getDept()
    {
        return $this->_Dept;
    }

    /**
     * Sets column aqg_aq_pq
     *
     * @param string $data
     * @return Application_Model_Faculty
     */
    public function setAqgAqPq($data)
    {
        $this->_AqgAqPq = $data;
        return $this;
    }

    /**
     * Gets column aqg_aq_pq
     *
     * @return string
     */
    public function getAqgAqPq()
    {
        return $this->_AqgAqPq;
    }

    /**
     * Sets column p_s
     *
     * @param string $data
     * @return Application_Model_Faculty
     */
    public function setPS($data)
    {
        $this->_PS = $data;
        return $this;
    }

    /**
     * Gets column p_s
     *
     * @return string
     */
    public function getPS()
    {
        return $this->_PS;
    }

    /**
     * Sets column date_hired
     *
     * @param string $data
     * @return Application_Model_Faculty
     */
    public function setDateHired($data)
    {
        $this->_DateHired = $data;
        return $this;
    }

    /**
     * Gets column date_hired
     *
     * @return string
     */
    public function getDateHired()
    {
        return $this->_DateHired;
    }

    /**
     * Sets dependent relations ic_ibfk_1
     *
     * @param array $data An array of Application_Model_Ic
     * @return Application_Model_Faculty
     */
    public function setIc(array $data)
    {
        $this->_Ic = array();

        foreach ($data as $object) {
            $this->addIc($object);
        }

        return $this;
    }

    /**
     * Sets dependent relations ic_ibfk_1
     *
     * @param Application_Model_Ic $data
     * @return Application_Model_Faculty
     */
    public function addIc(Application_Model_Ic $data)
    {
        $this->_Ic[] = $data;
        return $this;
    }

    /**
     * Gets dependent ic_ibfk_1
     *
     * @param boolean $load Load the object if it is not already
     * @return array The array of Application_Model_Ic
     */
    public function getIc($load = true)
    {
        if ($this->_Ic === null && $load) {
            $this->getMapper()->loadRelated('IcIbfk1', $this);
        }

        return $this->_Ic;
    }

    /**
     * Returns the mapper class for this model
     *
     * @return Application_Model_Mapper_Faculty
     */
    public function getMapper()
    {
        if ($this->_mapper === null) {
            $this->setMapper(new Application_Model_Mapper_Faculty());
        }

        return $this->_mapper;
    }

    /**
     * Deletes current row by deleting the row that matches the primary key
     *
	 * @see Application_Model_Mapper_Faculty::delete
     * @return int|boolean Number of rows deleted or boolean if doing soft delete
     */
    public function deleteRowByPrimaryKey()
    {
        if ($this->getFid() === null) {
            throw new Exception('Primary Key does not contain a value');
        }

        return $this->getMapper()
                    ->getDbTable()
                    ->delete('fid = ' .
                             $this->getMapper()
                                  ->getDbTable()
                                  ->getAdapter()
                                  ->quote($this->getFid()));
    }
}
