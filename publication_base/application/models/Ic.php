<?php

/**
 * Application Models
 *
 * @package Application_Model
 * @subpackage Model
 * @author Florent De Neve
 * @copyright 
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */


/**
 * 
 *
 * @package Application_Model
 * @subpackage Model
 * @author Florent De Neve
 */
class Application_Model_Ic extends Application_Model_ModelAbstract
{

    /**
     * Database var type int(11)
     *
     * @var int
     */
    protected $_Id;

    /**
     * Database var type int(11)
     *
     * @var int
     */
    protected $_FacultyFid;

    /**
     * Database var type varchar(255)
     *
     * @var string
     */
    protected $_ResearchName;

    /**
     * Database var type varchar(255)
     *
     * @var string
     */
    protected $_Title;

    /**
     * Database var type year(4)
     *
     * @var year
     */
    protected $_Year;


    /**
     * Parent relation ic_ibfk_1
     *
     * @var Application_Model_Faculty
     */
    protected $_FacultyF;


    /**
     * Sets up column and relationship lists
     */
    public function __construct()
    {
        parent::init();
        $this->setColumnsList(array(
            'id'=>'Id',
            'faculty_fid'=>'FacultyFid',
            'research_name'=>'ResearchName',
            'title'=>'Title',
            'year'=>'Year',
        ));

        $this->setParentList(array(
            'IcIbfk1'=> array(
                    'property' => 'FacultyF',
                    'table_name' => 'Faculty',
                ),
        ));

        $this->setDependentList(array(
        ));
    }

    /**
     * Sets column id
     *
     * @param int $data
     * @return Application_Model_Ic
     */
    public function setId($data)
    {
        $this->_Id = $data;
        return $this;
    }

    /**
     * Gets column id
     *
     * @return int
     */
    public function getId()
    {
        return $this->_Id;
    }

    /**
     * Sets column faculty_fid
     *
     * @param int $data
     * @return Application_Model_Ic
     */
    public function setFacultyFid($data)
    {
        $this->_FacultyFid = $data;
        return $this;
    }

    /**
     * Gets column faculty_fid
     *
     * @return int
     */
    public function getFacultyFid()
    {
        return $this->_FacultyFid;
    }

    /**
     * Sets column research_name
     *
     * @param string $data
     * @return Application_Model_Ic
     */
    public function setResearchName($data)
    {
        $this->_ResearchName = $data;
        return $this;
    }

    /**
     * Gets column research_name
     *
     * @return string
     */
    public function getResearchName()
    {
        return $this->_ResearchName;
    }

    /**
     * Sets column title
     *
     * @param string $data
     * @return Application_Model_Ic
     */
    public function setTitle($data)
    {
        $this->_Title = $data;
        return $this;
    }

    /**
     * Gets column title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_Title;
    }

    /**
     * Sets column year
     *
     * @param year $data
     * @return Application_Model_Ic
     */
    public function setYear($data)
    {
        $this->_Year = $data;
        return $this;
    }

    /**
     * Gets column year
     *
     * @return year
     */
    public function getYear()
    {
        return $this->_Year;
    }

    /**
     * Sets parent relation FacultyF
     *
     * @param Application_Model_Faculty $data
     * @return Application_Model_Ic
     */
    public function setFacultyF(Application_Model_Faculty $data)
    {
        $this->_FacultyF = $data;

        $primary_key = $data->getPrimaryKey();
        if (is_array($primary_key)) {
            $primary_key = $primary_key['fid'];
        }

        $this->setFacultyFid($primary_key);

        return $this;
    }

    /**
     * Gets parent FacultyF
     *
     * @param boolean $load Load the object if it is not already
     * @return Application_Model_Faculty
     */
    public function getFacultyF($load = true)
    {
        if ($this->_FacultyF === null && $load) {
            $this->getMapper()->loadRelated('IcIbfk1', $this);
        }

        return $this->_FacultyF;
    }

    /**
     * Returns the mapper class for this model
     *
     * @return Application_Model_Mapper_Ic
     */
    public function getMapper()
    {
        if ($this->_mapper === null) {
            $this->setMapper(new Application_Model_Mapper_Ic());
        }

        return $this->_mapper;
    }

    /**
     * Deletes current row by deleting the row that matches the primary key
     *
	 * @see Application_Model_Mapper_Ic::delete
     * @return int|boolean Number of rows deleted or boolean if doing soft delete
     */
    public function deleteRowByPrimaryKey()
    {
        if ($this->getId() === null) {
            throw new Exception('Primary Key does not contain a value');
        }

        return $this->getMapper()
                    ->getDbTable()
                    ->delete('id = ' .
                             $this->getMapper()
                                  ->getDbTable()
                                  ->getAdapter()
                                  ->quote($this->getId()));
    }
}
