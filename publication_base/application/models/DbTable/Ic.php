<?php

/**
 * Application Model DbTables
 *
 * @package Application_Model
 * @subpackage DbTable
 * @author Florent De Neve
 * @copyright 
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */

/**
 * Table definition for ic
 *
 * @package Application_Model
 * @subpackage DbTable
 * @author Florent De Neve
 */
class Application_Model_DbTable_Ic extends Application_Model_DbTable_TableAbstract
{
    /**
     * $_name - name of database table
     *
     * @var string
     */
    protected $_name = 'ic';

    /**
     * $_id - this is the primary key name
     *
     * @var int
     */
    protected $_id = 'id';

    protected $_sequence = true;

    protected $_referenceMap = array(
        'IcIbfk1' => array(
          	'columns' => 'faculty_fid',
            'refTableClass' => 'Application_Model_DbTable_Faculty',
            'refColumns' => 'fid'
        )
    );
    



}
