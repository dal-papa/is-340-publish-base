<?php

/**
 * Application Model DbTables
 *
 * @package Application_Model
 * @subpackage DbTable
 * @author Florent De Neve
 * @copyright 
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */

/**
 * Table definition for research
 *
 * @package Application_Model
 * @subpackage DbTable
 * @author Florent De Neve
 */
class Application_Model_DbTable_Research extends Application_Model_DbTable_TableAbstract
{
    /**
     * $_name - name of database table
     *
     * @var string
     */
    protected $_name = 'research';

    /**
     * $_id - this is the primary key name
     *
     * @var int
     */
    protected $_id = 'rid';

    protected $_sequence = true;

    
    protected $_dependentTables = array(
        'Ssci'
    );



}
