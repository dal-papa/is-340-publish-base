<?php

/**
 * Application Model DbTables
 *
 * @package Application_Model
 * @subpackage DbTable
 * @author Florent De Neve
 * @copyright 
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */

/**
 * Table definition for faculty
 *
 * @package Application_Model
 * @subpackage DbTable
 * @author Florent De Neve
 */
class Application_Model_DbTable_Faculty extends Application_Model_DbTable_TableAbstract
{
    /**
     * $_name - name of database table
     *
     * @var string
     */
    protected $_name = 'faculty';

    /**
     * $_id - this is the primary key name
     *
     * @var int
     */
    protected $_id = 'fid';

    protected $_sequence = true;

    
    protected $_dependentTables = array(
        'Ic'
    );



}
