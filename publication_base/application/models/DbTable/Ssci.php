<?php

/**
 * Application Model DbTables
 *
 * @package Application_Model
 * @subpackage DbTable
 * @author Florent De Neve
 * @copyright 
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */

/**
 * Table definition for ssci
 *
 * @package Application_Model
 * @subpackage DbTable
 * @author Florent De Neve
 */
class Application_Model_DbTable_Ssci extends Application_Model_DbTable_TableAbstract
{
    /**
     * $_name - name of database table
     *
     * @var string
     */
    protected $_name = 'ssci';

    /**
     * $_id - this is the primary key name
     *
     * @var int
     */
    protected $_id = 'research_rid';

    protected $_sequence = true;

    protected $_referenceMap = array(
        'SsciIbfk1' => array(
          	'columns' => 'research_rid',
            'refTableClass' => 'Application_Model_DbTable_Research',
            'refColumns' => 'rid'
        )
    );
    



}
