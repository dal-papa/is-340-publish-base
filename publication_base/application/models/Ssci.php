<?php

/**
 * Application Models
 *
 * @package Application_Model
 * @subpackage Model
 * @author Florent De Neve
 * @copyright 
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */


/**
 * 
 *
 * @package Application_Model
 * @subpackage Model
 * @author Florent De Neve
 */
class Application_Model_Ssci extends Application_Model_ModelAbstract
{

    /**
     * Database var type float
     *
     * @var float
     */
    protected $_Ssci2003;

    /**
     * Database var type float
     *
     * @var float
     */
    protected $_Ssci2004;

    /**
     * Database var type float
     *
     * @var float
     */
    protected $_Ssci2005;

    /**
     * Database var type float
     *
     * @var float
     */
    protected $_Ssci2006;

    /**
     * Database var type float
     *
     * @var float
     */
    protected $_Ssci2007;

    /**
     * Database var type float
     *
     * @var float
     */
    protected $_Ssci2008;

    /**
     * Database var type float
     *
     * @var float
     */
    protected $_Ssci2009;

    /**
     * Database var type float
     *
     * @var float
     */
    protected $_Ssci20095year;

    /**
     * Database var type int(11)
     *
     * @var int
     */
    protected $_ResearchRid;


    /**
     * Parent relation ssci_ibfk_1
     *
     * @var Application_Model_Research
     */
    protected $_ResearchR;


    /**
     * Sets up column and relationship lists
     */
    public function __construct()
    {
        parent::init();
        $this->setColumnsList(array(
            'ssci_2003'=>'Ssci2003',
            'ssci_2004'=>'Ssci2004',
            'ssci_2005'=>'Ssci2005',
            'ssci_2006'=>'Ssci2006',
            'ssci_2007'=>'Ssci2007',
            'ssci_2008'=>'Ssci2008',
            'ssci_2009'=>'Ssci2009',
            'ssci_2009_5year'=>'Ssci20095year',
            'research_rid'=>'ResearchRid',
        ));

        $this->setParentList(array(
            'SsciIbfk1'=> array(
                    'property' => 'ResearchR',
                    'table_name' => 'Research',
                ),
        ));

        $this->setDependentList(array(
        ));
    }

    /**
     * Sets column ssci_2003
     *
     * @param float $data
     * @return Application_Model_Ssci
     */
    public function setSsci2003($data)
    {
        $this->_Ssci2003 = $data;
        return $this;
    }

    /**
     * Gets column ssci_2003
     *
     * @return float
     */
    public function getSsci2003()
    {
        return $this->_Ssci2003;
    }

    /**
     * Sets column ssci_2004
     *
     * @param float $data
     * @return Application_Model_Ssci
     */
    public function setSsci2004($data)
    {
        $this->_Ssci2004 = $data;
        return $this;
    }

    /**
     * Gets column ssci_2004
     *
     * @return float
     */
    public function getSsci2004()
    {
        return $this->_Ssci2004;
    }

    /**
     * Sets column ssci_2005
     *
     * @param float $data
     * @return Application_Model_Ssci
     */
    public function setSsci2005($data)
    {
        $this->_Ssci2005 = $data;
        return $this;
    }

    /**
     * Gets column ssci_2005
     *
     * @return float
     */
    public function getSsci2005()
    {
        return $this->_Ssci2005;
    }

    /**
     * Sets column ssci_2006
     *
     * @param float $data
     * @return Application_Model_Ssci
     */
    public function setSsci2006($data)
    {
        $this->_Ssci2006 = $data;
        return $this;
    }

    /**
     * Gets column ssci_2006
     *
     * @return float
     */
    public function getSsci2006()
    {
        return $this->_Ssci2006;
    }

    /**
     * Sets column ssci_2007
     *
     * @param float $data
     * @return Application_Model_Ssci
     */
    public function setSsci2007($data)
    {
        $this->_Ssci2007 = $data;
        return $this;
    }

    /**
     * Gets column ssci_2007
     *
     * @return float
     */
    public function getSsci2007()
    {
        return $this->_Ssci2007;
    }

    /**
     * Sets column ssci_2008
     *
     * @param float $data
     * @return Application_Model_Ssci
     */
    public function setSsci2008($data)
    {
        $this->_Ssci2008 = $data;
        return $this;
    }

    /**
     * Gets column ssci_2008
     *
     * @return float
     */
    public function getSsci2008()
    {
        return $this->_Ssci2008;
    }

    /**
     * Sets column ssci_2009
     *
     * @param float $data
     * @return Application_Model_Ssci
     */
    public function setSsci2009($data)
    {
        $this->_Ssci2009 = $data;
        return $this;
    }

    /**
     * Gets column ssci_2009
     *
     * @return float
     */
    public function getSsci2009()
    {
        return $this->_Ssci2009;
    }

    /**
     * Sets column ssci_2009_5year
     *
     * @param float $data
     * @return Application_Model_Ssci
     */
    public function setSsci20095year($data)
    {
        $this->_Ssci20095year = $data;
        return $this;
    }

    /**
     * Gets column ssci_2009_5year
     *
     * @return float
     */
    public function getSsci20095year()
    {
        return $this->_Ssci20095year;
    }

    /**
     * Sets column research_rid
     *
     * @param int $data
     * @return Application_Model_Ssci
     */
    public function setResearchRid($data)
    {
        $this->_ResearchRid = $data;
        return $this;
    }

    /**
     * Gets column research_rid
     *
     * @return int
     */
    public function getResearchRid()
    {
        return $this->_ResearchRid;
    }

    /**
     * Sets parent relation ResearchR
     *
     * @param Application_Model_Research $data
     * @return Application_Model_Ssci
     */
    public function setResearchR(Application_Model_Research $data)
    {
        $this->_ResearchR = $data;

        $primary_key = $data->getPrimaryKey();
        if (is_array($primary_key)) {
            $primary_key = $primary_key['rid'];
        }

        $this->setResearchRid($primary_key);

        return $this;
    }

    /**
     * Gets parent ResearchR
     *
     * @param boolean $load Load the object if it is not already
     * @return Application_Model_Research
     */
    public function getResearchR($load = true)
    {
        if ($this->_ResearchR === null && $load) {
            $this->getMapper()->loadRelated('SsciIbfk1', $this);
        }

        return $this->_ResearchR;
    }

    /**
     * Returns the mapper class for this model
     *
     * @return Application_Model_Mapper_Ssci
     */
    public function getMapper()
    {
        if ($this->_mapper === null) {
            $this->setMapper(new Application_Model_Mapper_Ssci());
        }

        return $this->_mapper;
    }

    /**
     * Deletes current row by deleting the row that matches the primary key
     *
	 * @see Application_Model_Mapper_Ssci::delete
     * @return int|boolean Number of rows deleted or boolean if doing soft delete
     */
    public function deleteRowByPrimaryKey()
    {
        if ($this->getResearchRid() === null) {
            throw new Exception('Primary Key does not contain a value');
        }

        return $this->getMapper()
                    ->getDbTable()
                    ->delete('research_rid = ' .
                             $this->getMapper()
                                  ->getDbTable()
                                  ->getAdapter()
                                  ->quote($this->getResearchRid()));
    }
}
