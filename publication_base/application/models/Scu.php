<?php

/**
 * Application Models
 *
 * @package Application_Model
 * @subpackage Model
 * @author Florent De Neve
 * @copyright 
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */


/**
 * 
 *
 * @package Application_Model
 * @subpackage Model
 * @author Florent De Neve
 */
class Application_Model_Scu extends Application_Model_ModelAbstract
{

    /**
     * Database var type int(11)
     *
     * @var int
     */
    protected $_Id;

    /**
     * Database var type year(4)
     *
     * @var year
     */
    protected $_Year;

    /**
     * Database var type enum('spring','fall','summer','winter')
     *
     * @var string
     */
    protected $_Term;

    /**
     * Database var type int(11)
     *
     * @var int
     */
    protected $_Scu;

    /**
     * Database var type enum('mba','u','amba')
     *
     * @var string
     */
    protected $_Program;

    /**
     * Database var type int(11)
     *
     * @var int
     */
    protected $_FacultyFid;



    /**
     * Sets up column and relationship lists
     */
    public function __construct()
    {
        parent::init();
        $this->setColumnsList(array(
            'id'=>'Id',
            'year'=>'Year',
            'term'=>'Term',
            'scu'=>'Scu',
            'program'=>'Program',
            'faculty_fid'=>'FacultyFid',
        ));

        $this->setParentList(array(
        ));

        $this->setDependentList(array(
        ));
    }

    /**
     * Sets column id
     *
     * @param int $data
     * @return Application_Model_Scu
     */
    public function setId($data)
    {
        $this->_Id = $data;
        return $this;
    }

    /**
     * Gets column id
     *
     * @return int
     */
    public function getId()
    {
        return $this->_Id;
    }

    /**
     * Sets column year
     *
     * @param year $data
     * @return Application_Model_Scu
     */
    public function setYear($data)
    {
        $this->_Year = $data;
        return $this;
    }

    /**
     * Gets column year
     *
     * @return year
     */
    public function getYear()
    {
        return $this->_Year;
    }

    /**
     * Sets column term
     *
     * @param string $data
     * @return Application_Model_Scu
     */
    public function setTerm($data)
    {
        $this->_Term = $data;
        return $this;
    }

    /**
     * Gets column term
     *
     * @return string
     */
    public function getTerm()
    {
        return $this->_Term;
    }

    /**
     * Sets column scu
     *
     * @param int $data
     * @return Application_Model_Scu
     */
    public function setScu($data)
    {
        $this->_Scu = $data;
        return $this;
    }

    /**
     * Gets column scu
     *
     * @return int
     */
    public function getScu()
    {
        return $this->_Scu;
    }

    /**
     * Sets column program
     *
     * @param string $data
     * @return Application_Model_Scu
     */
    public function setProgram($data)
    {
        $this->_Program = $data;
        return $this;
    }

    /**
     * Gets column program
     *
     * @return string
     */
    public function getProgram()
    {
        return $this->_Program;
    }

    /**
     * Sets column faculty_fid
     *
     * @param int $data
     * @return Application_Model_Scu
     */
    public function setFacultyFid($data)
    {
        $this->_FacultyFid = $data;
        return $this;
    }

    /**
     * Gets column faculty_fid
     *
     * @return int
     */
    public function getFacultyFid()
    {
        return $this->_FacultyFid;
    }

    /**
     * Returns the mapper class for this model
     *
     * @return Application_Model_Mapper_Scu
     */
    public function getMapper()
    {
        if ($this->_mapper === null) {
            $this->setMapper(new Application_Model_Mapper_Scu());
        }

        return $this->_mapper;
    }

    /**
     * Deletes current row by deleting the row that matches the primary key
     *
	 * @see Application_Model_Mapper_Scu::delete
     * @return int|boolean Number of rows deleted or boolean if doing soft delete
     */
    public function deleteRowByPrimaryKey()
    {
        if ($this->getId() === null) {
            throw new Exception('Primary Key does not contain a value');
        }

        return $this->getMapper()
                    ->getDbTable()
                    ->delete('id = ' .
                             $this->getMapper()
                                  ->getDbTable()
                                  ->getAdapter()
                                  ->quote($this->getId()));
    }

}
