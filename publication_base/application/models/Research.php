<?php

/**
 * Application Models
 *
 * @package Application_Model
 * @subpackage Model
 * @author Florent De Neve
 * @copyright 
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */


/**
 * 
 *
 * @package Application_Model
 * @subpackage Model
 * @author Florent De Neve
 */
class Application_Model_Research extends Application_Model_ModelAbstract
{

    /**
     * Database var type int(11)
     *
     * @var int
     */
    protected $_Rid;

    /**
     * Database var type varchar(255)
     *
     * @var string
     */
    protected $_ResearchName;

    /**
     * Database var type varchar(50)
     *
     * @var string
     */
    protected $_Discipline;

    /**
     * Database var type varchar(30)
     *
     * @var string
     */
    protected $_Classification;

    /**
     * Database var type time
     *
     * @var time
     */
    protected $_DateAdded;

    /**
     * Database var type varchar(30)
     *
     * @var string
     */
    protected $_PriorClassification;

    /**
     * Database var type int(11)
     *
     * @var int
     */
    protected $_HIndex;

    /**
     * Database var type int(11)
     *
     * @var int
     */
    protected $_GIndex;

    /**
     * Database var type int(11)
     *
     * @var int
     */
    protected $_AbsRank;

    /**
     * Database var type varchar(5)
     *
     * @var string
     */
    protected $_AbdcRank;

    /**
     * Database var type int(11)
     *
     * @var int
     */
    protected $_StartYear;

    /**
     * Database var type float
     *
     * @var float
     */
    protected $_AverageCites;



    /**
     * Dependent relation ssci_ibfk_1
     * Type: One-to-One relationship
     *
     * @var Application_Model_Ssci
     */
    protected $_Ssci;

    /**
     * Sets up column and relationship lists
     */
    public function __construct()
    {
        parent::init();
        $this->setColumnsList(array(
            'rid'=>'Rid',
            'research_name'=>'ResearchName',
            'discipline'=>'Discipline',
            'classification'=>'Classification',
            'date_added'=>'DateAdded',
            'prior_classification'=>'PriorClassification',
            'h_index'=>'HIndex',
            'g_index'=>'GIndex',
            'abs_rank'=>'AbsRank',
            'abdc_rank'=>'AbdcRank',
            'start_year'=>'StartYear',
            'average_cites'=>'AverageCites',
        ));

        $this->setParentList(array(
        ));

        $this->setDependentList(array(
            'SsciIbfk1' => array(
                    'property' => 'Ssci',
                    'table_name' => 'Ssci',
                ),
        ));
    }

    /**
     * Sets column rid
     *
     * @param int $data
     * @return Application_Model_Research
     */
    public function setRid($data)
    {
        $this->_Rid = $data;
        return $this;
    }

    /**
     * Gets column rid
     *
     * @return int
     */
    public function getRid()
    {
        return $this->_Rid;
    }

    /**
     * Sets column research_name
     *
     * @param string $data
     * @return Application_Model_Research
     */
    public function setResearchName($data)
    {
        $this->_ResearchName = $data;
        return $this;
    }

    /**
     * Gets column research_name
     *
     * @return string
     */
    public function getResearchName()
    {
        return $this->_ResearchName;
    }

    /**
     * Sets column discipline
     *
     * @param string $data
     * @return Application_Model_Research
     */
    public function setDiscipline($data)
    {
        $this->_Discipline = $data;
        return $this;
    }

    /**
     * Gets column discipline
     *
     * @return string
     */
    public function getDiscipline()
    {
        return $this->_Discipline;
    }

    /**
     * Sets column classification
     *
     * @param string $data
     * @return Application_Model_Research
     */
    public function setClassification($data)
    {
        $this->_Classification = $data;
        return $this;
    }

    /**
     * Gets column classification
     *
     * @return string
     */
    public function getClassification()
    {
        return $this->_Classification;
    }

    /**
     * Sets column date_added
     *
     * @param time $data
     * @return Application_Model_Research
     */
    public function setDateAdded($data)
    {
        $this->_DateAdded = $data;
        return $this;
    }

    /**
     * Gets column date_added
     *
     * @return time
     */
    public function getDateAdded()
    {
        return $this->_DateAdded;
    }

    /**
     * Sets column prior_classification
     *
     * @param string $data
     * @return Application_Model_Research
     */
    public function setPriorClassification($data)
    {
        $this->_PriorClassification = $data;
        return $this;
    }

    /**
     * Gets column prior_classification
     *
     * @return string
     */
    public function getPriorClassification()
    {
        return $this->_PriorClassification;
    }

    /**
     * Sets column h_index
     *
     * @param int $data
     * @return Application_Model_Research
     */
    public function setHIndex($data)
    {
        $this->_HIndex = $data;
        return $this;
    }

    /**
     * Gets column h_index
     *
     * @return int
     */
    public function getHIndex()
    {
        return $this->_HIndex;
    }

    /**
     * Sets column g_index
     *
     * @param int $data
     * @return Application_Model_Research
     */
    public function setGIndex($data)
    {
        $this->_GIndex = $data;
        return $this;
    }

    /**
     * Gets column g_index
     *
     * @return int
     */
    public function getGIndex()
    {
        return $this->_GIndex;
    }

    /**
     * Sets column abs_rank
     *
     * @param int $data
     * @return Application_Model_Research
     */
    public function setAbsRank($data)
    {
        $this->_AbsRank = $data;
        return $this;
    }

    /**
     * Gets column abs_rank
     *
     * @return int
     */
    public function getAbsRank()
    {
        return $this->_AbsRank;
    }

    /**
     * Sets column abdc_rank
     *
     * @param string $data
     * @return Application_Model_Research
     */
    public function setAbdcRank($data)
    {
        $this->_AbdcRank = $data;
        return $this;
    }

    /**
     * Gets column abdc_rank
     *
     * @return string
     */
    public function getAbdcRank()
    {
        return $this->_AbdcRank;
    }

    /**
     * Sets column start_year
     *
     * @param int $data
     * @return Application_Model_Research
     */
    public function setStartYear($data)
    {
        $this->_StartYear = $data;
        return $this;
    }

    /**
     * Gets column start_year
     *
     * @return int
     */
    public function getStartYear()
    {
        return $this->_StartYear;
    }

    /**
     * Sets column average_cites
     *
     * @param float $data
     * @return Application_Model_Research
     */
    public function setAverageCites($data)
    {
        $this->_AverageCites = $data;
        return $this;
    }

    /**
     * Gets column average_cites
     *
     * @return float
     */
    public function getAverageCites()
    {
        return $this->_AverageCites;
    }

    /**
     * Sets dependent relation ssci_ibfk_1
     *
     * @param Application_Model_Ssci $data
     * @return Application_Model_Research
     */
    public function setSsci(Application_Model_Ssci $data)
    {
        $this->_Ssci = $data;
        return $this;
    }

    /**
     * Gets dependent ssci_ibfk_1
     *
     * @param boolean $load Load the object if it is not already
     * @return Application_Model_Ssci
     */
    public function getSsci($load = true)
    {
        if ($this->_Ssci === null && $load) {
            $this->getMapper()->loadRelated('SsciIbfk1', $this);
        }

        return $this->_Ssci;
    }

    /**
     * Returns the mapper class for this model
     *
     * @return Application_Model_Mapper_Research
     */
    public function getMapper()
    {
        if ($this->_mapper === null) {
            $this->setMapper(new Application_Model_Mapper_Research());
        }

        return $this->_mapper;
    }

    /**
     * Deletes current row by deleting the row that matches the primary key
     *
	 * @see Application_Model_Mapper_Research::delete
     * @return int|boolean Number of rows deleted or boolean if doing soft delete
     */
    public function deleteRowByPrimaryKey()
    {
        if ($this->getRid() === null) {
            throw new Exception('Primary Key does not contain a value');
        }

        return $this->getMapper()
                    ->getDbTable()
                    ->delete('rid = ' .
                             $this->getMapper()
                                  ->getDbTable()
                                  ->getAdapter()
                                  ->quote($this->getRid()));
    }
}
