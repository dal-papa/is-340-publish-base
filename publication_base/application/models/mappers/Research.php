<?php

/**
 * Application Model Mappers
 *
 * @package Application_Model
 * @subpackage Mapper
 * @author Florent De Neve
 * @copyright 
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */

/**
 * Data Mapper implementation for Application_Model_Research
 *
 * @package Application_Model
 * @subpackage Mapper
 * @author Florent De Neve
 */
class Application_Model_Mapper_Research extends Application_Model_Mapper_MapperAbstract
{
    /**
     * Returns an array, keys are the field names.
     *
     * @param Application_Model_Research $model
     * @return array
     */
    public function toArray($model)
    {
        if (! $model instanceof Application_Model_Research) {
            throw new Exception('Unable to create array: invalid model passed to mapper');
        }

        $result = array(
            'rid' => $model->getRid(),
            'research_name' => $model->getResearchName(),
            'discipline' => $model->getDiscipline(),
            'classification' => $model->getClassification(),
            'date_added' => $model->getDateAdded(),
            'prior_classification' => $model->getPriorClassification(),
            'h_index' => $model->getHIndex(),
            'g_index' => $model->getGIndex(),
            'abs_rank' => $model->getAbsRank(),
            'abdc_rank' => $model->getAbdcRank(),
            'start_year' => $model->getStartYear(),
            'average_cites' => $model->getAverageCites(),
        );

        return $result;
    }

    /**
     * Returns the DbTable class associated with this mapper
     *
     * @return Application_Model_DbTable_Research
     */
    public function getDbTable()
    {
        if ($this->_dbTable === null) {
            $this->setDbTable('Application_Model_DbTable_Research');
        }

        return $this->_dbTable;
    }

    /**
     * Deletes the current model
     *
     * @param Application_Model_Research $model The model to delete
     * @see Application_Model_DbTable_TableAbstract::delete()
     * @return int
     */
    public function delete($model)
    {
        if (! $model instanceof Application_Model_Research) {
            throw new Exception('Unable to delete: invalid model passed to mapper');
        }

        $this->getDbTable()->getAdapter()->beginTransaction();
        try {
            $where = $this->getDbTable()->getAdapter()->quoteInto('rid = ?', $model->getRid());
            $result = $this->getDbTable()->delete($where);

            $this->getDbTable()->getAdapter()->commit();
        } catch (Exception $e) {
            $this->getDbTable()->getAdapter()->rollback();
            $result = false;
        }

        return $result;
    }

    /**
     * Saves current row, and optionally dependent rows
     *
     * @param Application_Model_Research $model
     * @param boolean $ignoreEmptyValues Should empty values saved
     * @param boolean $recursive Should the object graph be walked for all related elements
     * @param boolean $useTransaction Flag to indicate if save should be done inside a database transaction
     * @return boolean If the save action was successful
     */
    public function save(Application_Model_Research $model,
        $ignoreEmptyValues = true, $recursive = false, $useTransaction = true
    ) {
        $data = $model->toArray();
        if ($ignoreEmptyValues) {
            foreach ($data as $key => $value) {
                if ($value === null or $value === '') {
                    unset($data[$key]);
                }
            }
        }

        $primary_key = $model->getRid();
        $success = true;

        if ($useTransaction) {
            $this->getDbTable()->getAdapter()->beginTransaction();
        }

        unset($data['rid']);

        try {
            if ($primary_key === null) {
                $primary_key = $this->getDbTable()->insert($data);
                if ($primary_key) {
                    $model->setRid($primary_key);
                } else {
                    $success = false;
                }
            } else {
                $this->getDbTable()
                     ->update($data,
                              array(
                                 'rid = ?' => $primary_key
                              )
                );
            }

            if ($recursive) {
                if ($success && $model->getSsci(false) !== null) {
                    $success = $success &&
                        $model->getSsci()
                              ->setResearchRid($primary_key)
                              ->save($ignoreEmptyValues, $recursive, false);
                }

            }

            if ($useTransaction && $success) {
                $this->getDbTable()->getAdapter()->commit();
            } elseif ($useTransaction) {
                $this->getDbTable()->getAdapter()->rollback();
            }

        } catch (Exception $e) {
            if ($useTransaction) {
                $this->getDbTable()->getAdapter()->rollback();
            }

            $success = false;
        }

        return $success;
    }

    /**
     * Finds row by primary key
     *
     * @param int $primary_key
     * @param Application_Model_Research|null $model
     * @return Application_Model_Research|null The object provided or null if not found
     */
    public function find($primary_key, $model)
    {
        $result = $this->getRowset($primary_key);

        if (is_null($result)) {
            return null;
        }

        $row = $result->current();

        $model = $this->loadModel($row, $model);

        return $model;
    }

    /**
     * Loads the model specific data into the model object
     *
     * @param Zend_Db_Table_Row_Abstract|array $data The data as returned from a Zend_Db query
     * @param Application_Model_Research|null $entry The object to load the data into, or null to have one created
     * @return Application_Model_Research The model with the data provided
     */
    public function loadModel($data, $entry)
    {
        if ($entry === null) {
            $entry = new Application_Model_Research();
        }

        if (is_array($data)) {
            $entry->setRid($data['rid'])
                  ->setResearchName($data['research_name'])
                  ->setDiscipline($data['discipline'])
                  ->setClassification($data['classification'])
                  ->setDateAdded($data['date_added'])
                  ->setPriorClassification($data['prior_classification'])
                  ->setHIndex($data['h_index'])
                  ->setGIndex($data['g_index'])
                  ->setAbsRank($data['abs_rank'])
                  ->setAbdcRank($data['abdc_rank'])
                  ->setStartYear($data['start_year'])
                  ->setAverageCites($data['average_cites']);
        } elseif ($data instanceof Zend_Db_Table_Row_Abstract || $data instanceof stdClass) {
            $entry->setRid($data->rid)
                  ->setResearchName($data->research_name)
                  ->setDiscipline($data->discipline)
                  ->setClassification($data->classification)
                  ->setDateAdded($data->date_added)
                  ->setPriorClassification($data->prior_classification)
                  ->setHIndex($data->h_index)
                  ->setGIndex($data->g_index)
                  ->setAbsRank($data->abs_rank)
                  ->setAbdcRank($data->abdc_rank)
                  ->setStartYear($data->start_year)
                  ->setAverageCites($data->average_cites);
        }

        $entry->setMapper($this);

        return $entry;
    }
    
    /**
     * Return a list of full names searching by first and last name.
     * @param string $query
     * @param int $limit
     * @return array
     */
    public function fetchListOfNames($query, $limit)
    {
        $facultyTable = $this->getDbTable();
        $list = $facultyTable->fetchAll($facultyTable->select()
                ->where("research_name LIKE ?", "%$query%")
                ->limit($limit))->toArray();
        $listOfNames = array();
        foreach ($list as $value)
        {
            $listOfNames[] = $value['research_name'] .  " (" . $value["rid"] . ")";
        }
        return $listOfNames;
    }
}
