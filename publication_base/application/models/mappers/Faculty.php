<?php

/**
 * Application Model Mappers
 *
 * @package Application_Model
 * @subpackage Mapper
 * @author Florent De Neve
 * @copyright 
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */

/**
 * Data Mapper implementation for Application_Model_Faculty
 *
 * @package Application_Model
 * @subpackage Mapper
 * @author Florent De Neve
 */
class Application_Model_Mapper_Faculty extends Application_Model_Mapper_MapperAbstract
{
    /**
     * Returns an array, keys are the field names.
     *
     * @param Application_Model_Faculty $model
     * @return array
     */
    public function toArray($model)
    {
        if (! $model instanceof Application_Model_Faculty) {
            throw new Exception('Unable to create array: invalid model passed to mapper');
        }

        $result = array(
            'fid' => $model->getFid(),
            'last' => $model->getLast(),
            'first' => $model->getFirst(),
            'dob' => $model->getDob(),
            'rank' => $model->getRank(),
            'time_base' => $model->getTimeBase(),
            'dept' => $model->getDept(),
            'aqg_aq_pq' => $model->getAqgAqPq(),
            'p_s' => $model->getPS(),
            'date_hired' => $model->getDateHired(),
        );

        return $result;
    }

    /**
     * Returns the DbTable class associated with this mapper
     *
     * @return Application_Model_DbTable_Faculty
     */
    public function getDbTable()
    {
        if ($this->_dbTable === null) {
            $this->setDbTable('Application_Model_DbTable_Faculty');
        }

        return $this->_dbTable;
    }

    /**
     * Deletes the current model
     *
     * @param Application_Model_Faculty $model The model to delete
     * @see Application_Model_DbTable_TableAbstract::delete()
     * @return int
     */
    public function delete($model)
    {
        if (! $model instanceof Application_Model_Faculty) {
            throw new Exception('Unable to delete: invalid model passed to mapper');
        }

        $this->getDbTable()->getAdapter()->beginTransaction();
        try {
            $where = $this->getDbTable()->getAdapter()->quoteInto('fid = ?', $model->getFid());
            $result = $this->getDbTable()->delete($where);

            $this->getDbTable()->getAdapter()->commit();
        } catch (Exception $e) {
            $this->getDbTable()->getAdapter()->rollback();
            $result = false;
        }

        return $result;
    }

    /**
     * Saves current row, and optionally dependent rows
     *
     * @param Application_Model_Faculty $model
     * @param boolean $ignoreEmptyValues Should empty values saved
     * @param boolean $recursive Should the object graph be walked for all related elements
     * @param boolean $useTransaction Flag to indicate if save should be done inside a database transaction
     * @return boolean If the save action was successful
     */
    public function save(Application_Model_Faculty $model,
        $ignoreEmptyValues = true, $recursive = false, $useTransaction = true
    ) {
        $data = $model->toArray();
        if ($ignoreEmptyValues) {
            foreach ($data as $key => $value) {
                if ($value === null or $value === '') {
                    unset($data[$key]);
                }
            }
        }

        $primary_key = $model->getFid();
        $success = true;

        if ($useTransaction) {
            $this->getDbTable()->getAdapter()->beginTransaction();
        }

        unset($data['fid']);

        try {
            if ($primary_key === null) {
                $primary_key = $this->getDbTable()->insert($data);
                if ($primary_key) {
                    $model->setFid($primary_key);
                } else {
                    $success = false;
                }
            } else {
                $this->getDbTable()
                     ->update($data,
                              array(
                                 'fid = ?' => $primary_key
                              )
                );
            }

            if ($recursive) {
                if ($success && $model->getIc(false) !== null) {
                    $Ic = $model->getIc();
                    foreach ($Ic as $value) {
                        $success = $success &&
                            $value->setFacultyFid($primary_key)
                                  ->save($ignoreEmptyValues, $recursive, false);

                        if (! $success) {
                            break;
                        }
                    }
                }

            }

            if ($useTransaction && $success) {
                $this->getDbTable()->getAdapter()->commit();
            } elseif ($useTransaction) {
                $this->getDbTable()->getAdapter()->rollback();
            }

        } catch (Exception $e) {
            if ($useTransaction) {
                $this->getDbTable()->getAdapter()->rollback();
            }

            $success = false;
        }

        return $success;
    }

    /**
     * Finds row by primary key
     *
     * @param int $primary_key
     * @param Application_Model_Faculty|null $model
     * @return Application_Model_Faculty|null The object provided or null if not found
     */
    public function find($primary_key, $model)
    {
        $result = $this->getRowset($primary_key);

        if (is_null($result)) {
            return null;
        }

        $row = $result->current();

        $model = $this->loadModel($row, $model);

        return $model;
    }

    /**
     * Loads the model specific data into the model object
     *
     * @param Zend_Db_Table_Row_Abstract|array $data The data as returned from a Zend_Db query
     * @param Application_Model_Faculty|null $entry The object to load the data into, or null to have one created
     * @return Application_Model_Faculty The model with the data provided
     */
    public function loadModel($data, $entry)
    {
        if ($entry === null) {
            $entry = new Application_Model_Faculty();
        }

        if (is_array($data)) {
            $entry->setFid($data['fid'])
                  ->setLast($data['last'])
                  ->setFirst($data['first'])
                  ->setDob($data['dob'])
                  ->setRank($data['rank'])
                  ->setTimeBase($data['time_base'])
                  ->setDept($data['dept'])
                  ->setAqgAqPq($data['aqg_aq_pq'])
                  ->setPS($data['p_s'])
                  ->setDateHired($data['date_hired']);
        } elseif ($data instanceof Zend_Db_Table_Row_Abstract || $data instanceof stdClass) {
            $entry->setFid($data->fid)
                  ->setLast($data->last)
                  ->setFirst($data->first)
                  ->setDob($data->dob)
                  ->setRank($data->rank)
                  ->setTimeBase($data->time_base)
                  ->setDept($data->dept)
                  ->setAqgAqPq($data->aqg_aq_pq)
                  ->setPS($data->p_s)
                  ->setDateHired($data->date_hired);
        }

        $entry->setMapper($this);

        return $entry;
    }
    
    /**
     * Return a list of full names searching by first and last name.
     * @param string $query
     * @param int $limit
     * @return array
     */
    public function fetchListOfNames($query, $limit)
    {
        $facultyTable = $this->getDbTable();
        $list = $facultyTable->fetchAll($facultyTable->select()
                ->where("last LIKE ? OR first LIKE ?", "%$query%")
                ->limit($limit))->toArray();
        $listOfNames = array();
        foreach ($list as $value)
        {
            $listOfNames[] = $value['first'] . " " . $value['last'] . " (" . $value["fid"] . ")";
        }
        return $listOfNames;
    }
}
