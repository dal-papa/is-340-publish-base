<?php

/**
 * Application Model Mappers
 *
 * @package Application_Model
 * @subpackage Mapper
 * @author Florent De Neve
 * @copyright 
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */

/**
 * Data Mapper implementation for Application_Model_Ssci
 *
 * @package Application_Model
 * @subpackage Mapper
 * @author Florent De Neve
 */
class Application_Model_Mapper_Ssci extends Application_Model_Mapper_MapperAbstract
{
    /**
     * Returns an array, keys are the field names.
     *
     * @param Application_Model_Ssci $model
     * @return array
     */
    public function toArray($model)
    {
        if (! $model instanceof Application_Model_Ssci) {
            throw new Exception('Unable to create array: invalid model passed to mapper');
        }

        $result = array(
            'ssci_2003' => $model->getSsci2003(),
            'ssci_2004' => $model->getSsci2004(),
            'ssci_2005' => $model->getSsci2005(),
            'ssci_2006' => $model->getSsci2006(),
            'ssci_2007' => $model->getSsci2007(),
            'ssci_2008' => $model->getSsci2008(),
            'ssci_2009' => $model->getSsci2009(),
            'ssci_2009_5year' => $model->getSsci20095year(),
            'research_rid' => $model->getResearchRid(),
        );

        return $result;
    }

    /**
     * Returns the DbTable class associated with this mapper
     *
     * @return Application_Model_DbTable_Ssci
     */
    public function getDbTable()
    {
        if ($this->_dbTable === null) {
            $this->setDbTable('Application_Model_DbTable_Ssci');
        }

        return $this->_dbTable;
    }

    /**
     * Deletes the current model
     *
     * @param Application_Model_Ssci $model The model to delete
     * @see Application_Model_DbTable_TableAbstract::delete()
     * @return int
     */
    public function delete($model)
    {
        if (! $model instanceof Application_Model_Ssci) {
            throw new Exception('Unable to delete: invalid model passed to mapper');
        }

        $this->getDbTable()->getAdapter()->beginTransaction();
        try {
            $where = $this->getDbTable()->getAdapter()->quoteInto('research_rid = ?', $model->getResearchRid());
            $result = $this->getDbTable()->delete($where);

            $this->getDbTable()->getAdapter()->commit();
        } catch (Exception $e) {
            $this->getDbTable()->getAdapter()->rollback();
            $result = false;
        }

        return $result;
    }

    /**
     * Saves current row, and optionally dependent rows
     *
     * @param Application_Model_Ssci $model
     * @param boolean $ignoreEmptyValues Should empty values saved
     * @param boolean $recursive Should the object graph be walked for all related elements
     * @param boolean $useTransaction Flag to indicate if save should be done inside a database transaction
     * @return boolean If the save action was successful
     */
    public function save(Application_Model_Ssci $model,
        $ignoreEmptyValues = true, $recursive = false, $useTransaction = true
    ) {
        $data = $model->toArray();
        if ($ignoreEmptyValues) {
            foreach ($data as $key => $value) {
                if ($value === null or $value === '') {
                    unset($data[$key]);
                }
            }
        }

        $primary_key = $model->getResearchRid();
        $success = true;

        if ($useTransaction) {
            $this->getDbTable()->getAdapter()->beginTransaction();
        }

        $exists = $this->find($primary_key, null);

        try {
            if ($exists === null) {
                $primary_key = $this->getDbTable()->insert($data);
                if ($primary_key) {
                    $model->setResearchRid($primary_key);
                } else {
                    $success = false;
                }
            } else {
                $this->getDbTable()
                     ->update($data,
                              array(
                                 'research_rid = ?' => $primary_key
                              )
                );
            }

            if ($useTransaction && $success) {
                $this->getDbTable()->getAdapter()->commit();
            } elseif ($useTransaction) {
                $this->getDbTable()->getAdapter()->rollback();
            }

        } catch (Exception $e) {
            if ($useTransaction) {
                $this->getDbTable()->getAdapter()->rollback();
            }

            $success = false;
        }

        return $success;
    }

    /**
     * Finds row by primary key
     *
     * @param int $primary_key
     * @param Application_Model_Ssci|null $model
     * @return Application_Model_Ssci|null The object provided or null if not found
     */
    public function find($primary_key, $model)
    {
        $result = $this->getRowset($primary_key);

        if (is_null($result)) {
            return null;
        }

        $row = $result->current();

        $model = $this->loadModel($row, $model);

        return $model;
    }

    /**
     * Loads the model specific data into the model object
     *
     * @param Zend_Db_Table_Row_Abstract|array $data The data as returned from a Zend_Db query
     * @param Application_Model_Ssci|null $entry The object to load the data into, or null to have one created
     * @return Application_Model_Ssci The model with the data provided
     */
    public function loadModel($data, $entry)
    {
        if ($entry === null) {
            $entry = new Application_Model_Ssci();
        }

        if (is_array($data)) {
            $entry->setSsci2003($data['ssci_2003'])
                  ->setSsci2004($data['ssci_2004'])
                  ->setSsci2005($data['ssci_2005'])
                  ->setSsci2006($data['ssci_2006'])
                  ->setSsci2007($data['ssci_2007'])
                  ->setSsci2008($data['ssci_2008'])
                  ->setSsci2009($data['ssci_2009'])
                  ->setSsci20095year($data['ssci_2009_5year'])
                  ->setResearchRid($data['research_rid']);
        } elseif ($data instanceof Zend_Db_Table_Row_Abstract || $data instanceof stdClass) {
            $entry->setSsci2003($data->ssci_2003)
                  ->setSsci2004($data->ssci_2004)
                  ->setSsci2005($data->ssci_2005)
                  ->setSsci2006($data->ssci_2006)
                  ->setSsci2007($data->ssci_2007)
                  ->setSsci2008($data->ssci_2008)
                  ->setSsci2009($data->ssci_2009)
                  ->setSsci20095year($data->ssci_2009_5year)
                  ->setResearchRid($data->research_rid);
        }

        $entry->setMapper($this);

        return $entry;
    }
}
